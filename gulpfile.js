const gulp = require('gulp'),
    sass = require('gulp-sass')(require('sass')),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    del = require('del');

// const autoprefixer = require('gulp-autoprefixer');

gulp.task('clean', function() {
    return del.sync('dist')
});
gulp.task('sass', function() {
    return gulp.src('app/css/sass/**/*.scss', { sourcemap: true,})
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function() {
    return gulp.src('app/js/main/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('jQuery', function() {
    return gulp.src('app/js/jQuery/**/*.js')
        .pipe(concat('jQuery.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('marquee', function() {
    return gulp.src('app/js/marquee/**/*.js')
        .pipe(concat('marquee.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({stream: true}))
});


gulp.task('html', function() {
    return gulp.src('app/*.html')
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}))
});
gulp.task('img', function() {
    return gulp.src('app/media/img/**/*')
        .pipe(gulp.dest('dist/media/img'))
        .pipe(browserSync.reload({stream: true}))
});
gulp.task('video', function() {
    return gulp.src('app/media/video/**/*')
        .pipe(gulp.dest('dist/media/video'))
        .pipe(browserSync.reload({stream: true}))
});
gulp.task('svg', function() {
    return gulp.src('app/media/svg/**/*')
        .pipe(gulp.dest('dist/media/svg'))
        .pipe(browserSync.reload({stream: true}))
});
gulp.task('fonts', function() {
    return gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
        .pipe(browserSync.reload({stream: true}))
});
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'dist'
        },
        notify: false
    })
});
gulp.task('watch', function() {
    gulp.watch('app/css/**/*.scss', gulp.parallel('sass'))
    gulp.watch('app/*.html'), gulp.parallel('html')
    gulp.watch(['app/js/main/main.js']), gulp.parallel('scripts')
    gulp.watch(['app/js/jQuery/jQuery.js']), gulp.parallel('jQuery')
    gulp.watch(['app/js/marquee/marquee.js']), gulp.parallel('marquee')
    gulp.watch('app/img/**/*'), gulp.parallel('img')
    gulp.watch('app/video/**/*'), gulp.parallel('video')
    gulp.watch('app/fonts/**/*'), gulp.parallel('fonts')
    gulp.watch('app/svg/**/*'), gulp.parallel('svg')
    // gulp.task('default', gulp.parallel('sass','html', 'scripts', 'browser-sync','img', 'watch'))
})

gulp.task('build', gulp.parallel('clean','video','jQuery', 'marquee', 'svg', 'fonts', 'sass', 'scripts', 'html', 'img', 'browser-sync','watch'))